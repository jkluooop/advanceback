import Home from './page/home.vue'
import Panel from './page/panel.vue'
import Table from './page/table.vue'
import Modal from './page/modal.vue'
import Form from './page/form.vue'
import DraggablePanel from './page/draggable-panel.vue'
import Color from './page/color.vue'
import Tab from './page/tab.vue'
import Buttons from './page/buttons.vue'
import Editor from './page/Editor.vue'
import Test from './page/test.vue'

import FrameDoc from './page/doc/frame.vue'
import PageDoc from './page/doc/page.vue'
import NavbarDoc from './page/doc/navbar.vue'
import HomeDoc from './page/doc/home.vue'
import TableDoc from './page/doc/table.vue'

export default {
    '/home':{
        name: 'home',
        component: Home
    },
    '/panel':{
        name: 'panel',
        component: Panel
    },
    '/table':{
        name: 'table',
        component: Table
    },
    '/modal':{
        name: 'modal',
        component: Modal
    },
    '/form':{
        name: 'form',
        component: Form
    },
    '/tab':{
        name: 'tab',
        component: Tab
    },
    '/buttons':{
        name: 'buttons',
        component: Buttons
    },
    '/editor':{
        name: 'editor',
        component: Editor
    },
    '/ui/color':{
        name: 'color',
        component: Color
    },
    '/doc/frame':{
        name: 'doc-frame',
        component: FrameDoc
    },
    '/doc/page':{
        name: 'doc-page',
        component: PageDoc
    },
    '/doc/navbar':{
        name: 'doc-navbar',
        component: NavbarDoc
    },
    '/doc/home':{
        name: 'doc-home',
        component: HomeDoc
    },
    '/doc/table':{
        name: 'doc-table',
        component: TableDoc
    },
    '/test':{
        name: 'test',
        component: Test
    }
}