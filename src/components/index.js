import VForm from './form.vue'
import ImgPreview from './img-preview.vue'
import ImgUpload from './img-upload.vue'
import Login from './login.vue'
import MarkDoc from './mark-doc.vue'
import Modal from './modal.vue'
import Navbar from './navbar.vue'
import Note from './note.vue'
import Page from './page.vue'
import Panel from './panel.vue'
import QueryTable from './query-table.vue'
import VTable from './table.vue'
import Scroller from './scroller.vue'
import Tab from './tab.vue'
import TabItem from './tab-item.vue'
import Draggable from './draggable.vue'
import VEditor from './editor.vue'
import TimeSlot from './time-slot.vue'

const GAUI = {
    VForm,
    ImgPreview,
    ImgUpload,
    Login,
    MarkDoc,
    Modal,
    Navbar,
    Note,
    Page,
    Panel,
    QueryTable,
    VTable,
    Scroller,
    Tab,
    TabItem,
    Draggable,
    VEditor,
    TimeSlot
}

module.exports = GAUI